//http://totaki.com/poesiabinaria/2014/02/variables-compartidas-entre-procesos-hijos-en-c-fork/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <sys/mman.h>

#define MAX_BUF 1024
#define PORT 5004

//EGOR bonbila bakarra biadaltzea
// EGOR 12
//  zerbitzariak => OK 12 BAI RGB 100

struct erabiltzaile
{
	char izena[30];
	char pasahitza[30];
	int egoera[100][6];
		//0 indizean => bonbila zenb
		//1 indizean =>0 itzalita eta 1 piztuta
		//2 indizean => gorri kolorea
		//3 indizean => berde kolorea
		//4 indizean => urdin kolorea	
		//5 indizean => intentsitatea
	int *hasieratuta;
};

struct erabiltzaile e1;
struct erabiltzaile e2;
struct erabiltzaile e3;
struct erabiltzaile e4;


int komandoaIrakurri(char *buf){
	int i;
	char kom[6]; //Komando guztiak 5 tamaina daukate
	for(i = 0;i<5;i++)kom[i] = buf[i];
	//printf("%s\n", kom);
	if(strcmp("SARTU\0",kom) == 0)return 1;
	else if (strcmp("IRTEN",kom) == 0) return 2;
	else if (strcmp("PIZTU",kom) == 0) return 3;
	else if (strcmp("ITZAL",kom) == 0) return 4;
	else if (strcmp("KOLOR",kom) == 0) return 5;
	else if (strcmp("INDAR",kom) == 0) return 6;
	else if (strcmp("EGOER",kom) == 0) return 7;
	else return 0;
	
}

int erroreaBidali (int zenb, int sock)
{

	switch (zenb)
	{
		case 1: printf("---ER1---\n"); write(sock, "ER1\r\n", 5); break;//Komandoaren datuak gaizki.
		case 2: printf("---ER2---\n"); write(sock, "ER2\r\n", 5); break;//Errorea sartzerakoan edo bonbila ez aurkitu.
		case 3: printf("---ER3---\n"); write(sock, "ER3\r\n", 5); break;//Bonbila dagoeneko piztuta
		case 4: printf("---ER4---\n"); write(sock, "ER4\r\n", 5); break;//bonbila dagoeneko itzalduta.
		case 5: printf("---ER5---\n"); write(sock, "ER5\r\n", 5); break;//zenbaki desegokia[0,255]
		case 6: printf("---ER6---\n"); write(sock, "ER6\r\n", 5); break;//beste edozein komando.
		case 7: printf("---ER7---\n"); write(sock, "ER7\r\n", 5); break;//erabiltzailea okupatuta
	}

}



int piztu(char *buf, struct erabiltzaile *erab, int sock){


	char num[10];
	sprintf(num,"%s%s",&buf[5],&buf[6]);
	int bonbilaZenb = atoi(num);
	printf("* Piztu %d bonbila.[%s]\n", bonbilaZenb, erab->izena);

	int i, j;
	for (i=0;i<100;i++){
		//printf("%d---%d\n", i ,erab->egoera[i][0]);
		if(erab->egoera[i][0] == bonbilaZenb)
		{		
			if(erab->egoera[i][1] == 1)
			{
				erroreaBidali(3,sock);//piztuta dagoela berriro pizteko agindu da.
				return 1;
			}
			else
			{
				erab->egoera[i][1] = 1;
				write(sock, "OK\r\n", 4);
				return 0;
			}
		}	
	}
	erroreaBidali(2,sock);
	return 1;
}


int itzali(char *buf, struct erabiltzaile *erab, int sock){
	
	char num[10];
	sprintf(num,"%s%s",&buf[5],&buf[6]);
	int bonbilaZenb = atoi(num);
	printf("* Itzali %d bonbila.[%s]\n", bonbilaZenb, erab->izena );

	int i, j;
	for (i=0;i<100;i++)
	{
		if(erab->egoera[i][0] == bonbilaZenb)
		{		
			if(erab->egoera[i][1] == 0)
			{
				erroreaBidali(4,sock);//itzalita dagoela, berriro itzaltzeko agindu da.
				return 1;
			}
			else
			{
				erab->egoera[i][1] = 0;
				write(sock, "OK\r\n", 4);
				return 0;
			}
		}	
	}
	erroreaBidali(2,sock);
	return 1;
}


int kolorea(char *buf, struct erabiltzaile *erab, int sock){
	//Token link = http://goo.gl/ocEjhX 

	char *token = strtok (buf, " ");
	int i;
	int RGBBonbilarekin[4];

	//Banatu datuak bufferrean
	for (i=0;i<4;i++)
	{
		token = strtok(NULL, ";");
		RGBBonbilarekin[i] = atoi(token);	
	}
	printf("* %d bonbilaren kolorea aldatu (%d, %d, %d).[%s]\n", RGBBonbilarekin[3], RGBBonbilarekin[0], RGBBonbilarekin[1],RGBBonbilarekin[2], erab->izena );

	//Koloreak 0 eta 255 tartean daudela egiaztatzea
	for (i=0;i<3;i++){
		if(RGBBonbilarekin[i] > 255 || RGBBonbilarekin[i] < 0)
		{
			erroreaBidali(5,sock);
			return 1;
		}
	} //Iristen bada, ondo daude zenbakiak

	//Bonbilaren datuak aldatu
	for (i=0;i<100;i++){
		if(erab->egoera[i][0] == RGBBonbilarekin[3])
		{
			erab->egoera[i][2] = RGBBonbilarekin[0];
			erab->egoera[i][3] = RGBBonbilarekin[1];
			erab->egoera[i][4] = RGBBonbilarekin[2];
			write(sock, "OK\r\n", 4);
			return 0;
		}	
	}
	erroreaBidali(2,sock);
	return 1;
}

int indarra (char *buf, struct erabiltzaile *erab, int sock){
	
	char *token = strtok (buf, " ");
	int balioak[2];
	int i;

	//Banatu balioak bufferrekoak
	for (i=0;i<2;i++)
	{
		token = strtok(NULL, ";");
		balioak[i] = atoi(token);
		
	}
	printf("* Bonbila %d-ren intentsitatea = %d.[%s]\n", balioak[1], balioak[0], erab->izena);
	
	if(balioak[0] >100 || balioak[0] <0)
	{
		erroreaBidali(5,sock);
		return 1;
	}
	
	//Bonbila lortu
	for (i=0;i<100;i++){
		if(erab->egoera[i][0] == balioak[1])
		{
			erab->egoera[i][5] = balioak[0];
			write(sock, "OK\r\n", 4);
			return 0;
		}	
	}
	erroreaBidali(2,sock);
	return 1;
}

struct erabiltzaile * sartu (char *buf, int sock)
{

    char izena[30];
    char pasahitza[30];
 
    char *token;
    token = strtok(buf," ");
    token = strtok(NULL,";");
    strcpy(izena,token);
    token = strtok(NULL,"\r");
    strcpy(pasahitza,token);

    //erresgistratzeko aukerarik ez dagoenez, 4 baldintzarekin nahikoa dugu.
    if(strcmp(izena,e1.izena)==0 && strcmp(pasahitza,e1.pasahitza)==0)
    {
    	if(*e1.hasieratuta==1)
    	{
    		 printf("* Beste erabiltzaile bat saiatu da [%s] kontuan sartzen.\n",e1.izena); 
    		 erroreaBidali(7,sock); 
    		 return NULL;
    	}
    	printf("* %s sartu da.\n", izena);
        *e1.hasieratuta = 1;
        write(sock, "OK\r\n", 4);
        return &e1;
    	
    }
    else if (strcmp(izena,e2.izena)==0 && strcmp(pasahitza,e2.pasahitza)==0)
    {
		if(*e2.hasieratuta==1)
		{ 
			printf("* Beste erabiltzaile bat saiatu da [%s] kontuan sartzen.\n",e2.izena); 
			erroreaBidali(7,sock); 
			return NULL;
		}
    	printf("* %s sartu da.\n", izena);
        *e2.hasieratuta = 1;
        write(sock, "OK\r\n", 4);
        return &e2;
    }
    else if (strcmp(izena,e3.izena)==0 && strcmp(pasahitza,e3.pasahitza)==0)
    {
    	if(*e3.hasieratuta==1)
    	{ 
    		printf("* Beste erabiltzaile bat saiatu da [%s] kontuan sartzen.\n",e3.izena); 
    		erroreaBidali(7,sock); 
    		return NULL;
    	}
    	printf("* %s sartu da.\n", izena);
        *e3.hasieratuta = 1;
        write(sock, "OK\r\n", 4);
        return &e3;
    }
    else if (strcmp(izena,e4.izena)==0 && strcmp(pasahitza,e4.pasahitza)==0)
    {
    	if(*e4.hasieratuta==1){ 

    		printf("* Beste erabiltzaile bat saiatu da [%s] kontuan sartzen.\n",e4.izena); 
    		erroreaBidali(7,sock); 
    		return NULL;
    	}
    	printf("* %s sartu da.\n", izena);
        *e4.hasieratuta = 1;
        write(sock, "OK\r\n", 4);
        return &e4;
    }

    erroreaBidali(2,sock);
    return NULL;
 
}

int irten(struct erabiltzaile *erab, int sock)
{
	if(*erab->hasieratuta == 1)
	{
		printf("* %s irten da.\n", erab->izena);
		*erab->hasieratuta = 0;
		write(sock,"OK\r\n",4);
		return 0;
	}
	erroreaBidali(6,sock);
	return 1;//erabiltzailea konektatu gabe (printzipioz ezinezkoa, IRTEN komandoa bidaltzeko, sartuta egon behar baitu)
}

int egoera(char *buf, struct erabiltzaile erab, int sock){

	char bidaltzeko[25];//25 karaktere bidaliko dira gehienez.

	char num[10];
	sprintf(num,"%s%s",&buf[5],&buf[6]);
	int bonbilaZenb = atoi(num);
	printf("* %d. bonbilaren egoera bidali[%s]\n", bonbilaZenb, erab.izena);

	if(bonbilaZenb > 100 || bonbilaZenb < 0)
	{
		erroreaBidali(2,sock);
		return 1;
	}

	sprintf(bidaltzeko,"OK %d %d %d;%d;%d %d\r\n", erab.egoera[bonbilaZenb][0],//bonbila zenb
												   erab.egoera[bonbilaZenb][1],//itzali/piztu
												   erab.egoera[bonbilaZenb][2],//R
												   erab.egoera[bonbilaZenb][3],//G
												   erab.egoera[bonbilaZenb][4],//B
												   erab.egoera[bonbilaZenb][5]);//intentsitatea

	//printf("%s\n", bidaltzeko);
	write(sock,bidaltzeko,25);
	return 0;
}

int erabiltzaileakHasieratu()
{

	//Erabiltzaile 1 hasieratu "Beñat"
	strcpy(e1.izena,"Beñat");
	strcpy(e1.pasahitza,"123456");
	int i,j;
		
	for (i=0;i<100;i++){
		for(j=2;j<6;j++)
			e1.egoera[i][j] = 0;
	}
	for (i=0;i<100;i++)e1.egoera[i][0] = i;
	e1.hasieratuta = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);//Prozesu umeetan informazio globala izateko.
	*e1.hasieratuta = 0;

	//Erabiltzaile 2 hasieratu "Pello"
	strcpy(e2.izena,"Pello");
	strcpy(e2.pasahitza,"654321");
	for (i=0;i<100;i++){
		for(j=0;j<6;j++)
			e2.egoera[i][j] = 0;
	}
	for (i=0;i<100;i++)e2.egoera[i][0] = i;
	e2.hasieratuta = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);//Prozesu umeetan informazio globala izateko.
	*e2.hasieratuta = 0;

	//Erabiltzaile 3 hasieratu "Dani"
	strcpy(e3.izena,"Dani");
	strcpy(e3.pasahitza,"pacman");
	for (i=0;i<100;i++){
		for(j=0;j<6;j++)
			e3.egoera[i][j] = 0;
	}
	for (i=0;i<100;i++)e3.egoera[i][0] = i;
	e3.hasieratuta = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);//Prozesu umeetan informazio globala izateko.
	*e3.hasieratuta = 0;

	//Erabiltzaile 4 hasieratu "Unai"
	strcpy(e4.izena,"Unai");
	strcpy(e4.pasahitza,"batman");
	for (i=0;i<100;i++){
		for(j=0;j<6;j++)
			e4.egoera[i][j] = 0;
	}
	for (i=0;i<100;i++)e4.egoera[i][0] = i;
	e4.hasieratuta = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);//Prozesu umeetan informazio globala izateko.
	*e4.hasieratuta = 0;

	return 0;
}



int main()
{

	int sock, elkarrizketa, n, komZenb;
	struct sockaddr_in zerb_helb;
	char buf[MAX_BUF];
	int in = 0; //Ez dago barruan IN = BARRUAN

	struct erabiltzaile *erabiltzaileKonektatua;
	erabiltzaileakHasieratu();

	//Sortu TCP socketa	
	if((sock = socket(AF_INET, SOCK_STREAM,0)) < 0)
	{
		perror("Errorea socketa sortzean");
		exit(1);
	}
	
	//Esleitu helbidea 
	memset(&zerb_helb, 0, sizeof(zerb_helb));
	zerb_helb.sin_family = AF_INET;
	zerb_helb.sin_addr.s_addr = htonl(INADDR_ANY);
	zerb_helb.sin_port = htons(PORT);

	if(bind(sock, (struct sockaddr *) &zerb_helb, sizeof(zerb_helb)) < 0)
	{
		perror("Errorea helbidea esleitzean");
		exit(1);
	}
	
	//Entzute socketak sortu
	if(listen(sock,100) < 0) //Entzute sock = erreg.kop + 1
	{
		perror("Errorea entzute socketa sortzean");
		exit(1);
	}
	
	//Zombie egoerakoei kasu ez egitea
	signal(SIGCHLD, SIG_IGN);

	//Begizta infinitua
	while(1)
	{
		//Elkarrizketari deskriptore bat ezarri
		if((elkarrizketa = accept(sock, NULL, NULL)) < 0)
		{
			perror("Errorea konexioan");
			exit(1);
		}
		switch(fork())
		{
			case 0:	
			
				printf("%s\n", "BEZEROA KONEKTATUTA");
				close(sock);
				//Datuak irakurri
				while ((n=read(elkarrizketa,buf,MAX_BUF)) > 0)
				{
					buf[n]=0;
					switch(komandoaIrakurri(buf))
					{
						case 1: erabiltzaileKonektatua = sartu(buf,elkarrizketa);break; //Hasieratu
						case 2: irten(erabiltzaileKonektatua,elkarrizketa);break; //Log out
						case 3: piztu(buf,erabiltzaileKonektatua, elkarrizketa);break; // Bonbila piztu
						case 4: itzali(buf, erabiltzaileKonektatua, elkarrizketa); break; //Itzali bonbila
						case 5: kolorea(buf, erabiltzaileKonektatua, elkarrizketa); break; //Bonbila kolorea aldatu
						case 6: indarra(buf, erabiltzaileKonektatua, elkarrizketa ); break; //Intentsitatea aldatu
						case 7: egoera(buf, *erabiltzaileKonektatua, elkarrizketa); break; //Informazioa bidali
						default : erroreaBidali(6,elkarrizketa); break; //errore kodea bidali
					}
				}

			  	if(n < 0)
				{
					perror("Errorea datuak jasotzean");
					exit(1);
				}
				close(elkarrizketa);
				exit(0);	
		
			default: close(elkarrizketa);	
		}	
	}
}


