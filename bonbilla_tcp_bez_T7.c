#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#define MAX_BUF 1024
#define PORT 5004

void menua(char* erab){
	printf(" ");
	printf(" ");
	printf("////////////////////////////\n");
	printf("///// %s bezala logeatua////\n",erab);
	printf("////////////////////////////\n");
	printf("Aukeratu\n");
	printf("1 <- Bonbila piztu\n");
	printf("2 <- Bonbila itzali\n");
	printf("3 <- Bonbila baten kolorea aldatu\n");
	printf("4 <- Bonbila baten intentsitatea aldatu\n");
	printf("5 <- Bonbilen egoera inprimatu\n");
	printf("0 <- Saioa amaitu\n");
	printf(" ");
	printf(" ");
			
}

void idatzi(int socketa, char *buf)
{
	if(write(socketa, buf, strlen(buf)) < strlen(buf))
	{
		perror("Errorea datuak bidaltzean");
		close(socketa);
	}
}

int irakurri(int socketa, char *buf)
{
	int irak;
	if((irak = read(socketa, buf, MAX_BUF)) < 0)
	{
		perror("Errorea datuak jasotzean");
		close(socketa);
	}
	return irak;	
}
 

void main(int argc, char *argv[])
{
	int socketa, n, zenbakia, auk, error, R, G, B, ind, irten, elag, bonbzen;
	char buf[MAX_BUF], erab[MAX_BUF], pass[MAX_BUF], kom[MAX_BUF], er[MAX_BUF];
	char * KOMANDOAK[] ={"SARTU","IRTEN","PIZTU","ITZAL","KOLOR","INDAR","EGOER",NULL};
	char * ERANTZUNAK[]={"OK","ER2","ER3","ER4","ER5","ER6", "ER7", NULL};
	struct sockaddr_in zerb_helb;
	struct hostent *hp;
	char *token;
	char erantzun[25];
	

	if(argc != 2)
	{
		fprintf(stderr, "Erabilera: %s <zerbitzari izena | IPv4 helbidea>\n", argv[0]);
		exit(1);
	}
	
	if((socketa = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("Errorea socketa sortzean\n");
		exit(1);
	}

	memset(&zerb_helb, 0, sizeof(zerb_helb));
	zerb_helb.sin_family = AF_INET;
	zerb_helb.sin_port = htons(PORT);
	if((hp = gethostbyname(argv[1])) == NULL)
	{
		herror("Errorea zerbitzariaren izena ebaztean\n");
		exit(1);
	}
	memcpy(&zerb_helb.sin_addr, hp->h_addr, hp->h_length);

	if(connect(socketa, (struct sockaddr *) &zerb_helb, sizeof(zerb_helb)) < 0)
	{
		perror("Errorea zerbitzariarekin konektatzean\n");
		exit(1);
	}

	printf("Sar ezazu zure erabiltzailea:\n");
	fgets(erab,MAX_BUF,stdin);
	erab[strlen(erab)-1] = 0;
	printf("Sar ezazu pasahitza:\n");
	fgets(pass,MAX_BUF,stdin);

	pass[strlen(pass)-1] = 0;

	sprintf(buf,"%s %s;%s\r\n",KOMANDOAK[0],erab,pass);
	
	idatzi(socketa,buf);
	n=irakurri(socketa,buf);
	buf[n-2]=0;
	if(0==strncmp(ERANTZUNAK[0],buf,2))
	{
		irten=0;
		while(irten==0){
			menua(erab);
			printf(" ");
			scanf("%d",&auk);
			switch(auk){
				case 1:
					printf("Sar ezazu piztu nahi duzun argiaren zenbakia\n");
					scanf("%d",&zenbakia);
					sprintf(buf,"%s %d\r\n",KOMANDOAK[2],zenbakia);
					idatzi(socketa,buf);
					n=irakurri(socketa,buf);
					buf[n-2]=0;
					if(0==strncmp(ERANTZUNAK[0],buf,2)){
						printf(" ");
						printf("Argia piztu da!!!\n");
						printf(" ");
					}else if(0==strncmp(ERANTZUNAK[1],buf,3)){
						printf(" ");
						printf("Sartutako bonbila zenbakia ez da existitzen\n");
						printf(" ");
					}else if(0==strncmp(ERANTZUNAK[2],buf,3)){
						printf("Argia dagoeneko piztuta dago\n");
					}
					break;
				
				case 2:
					printf("Sar ezazu itzali nahi duzun argiaren zenbakia:\n");
					scanf("%d",&zenbakia);
					sprintf(buf,"%s %d\r\n",KOMANDOAK[3],zenbakia);
					idatzi(socketa,buf);	
					n=irakurri(socketa,buf);
					buf[n-2]=0;
					if(0==strncmp(ERANTZUNAK[0],buf,2))
					{
						printf("Bonbila itzali da\n");
					}else if(0==strncmp(ERANTZUNAK[1],buf,3)){
						printf("Sartutako bonbila zenbakia ez da existitzen D:\n");
					}else if(0==strncmp(ERANTZUNAK[3],buf,3)){
						printf("Bonbila dagoneko itzalita dago\n");
					}
					break;
			
				case 3:
					printf("Sartu aldatu nahi duzun argiaren zenbakia:\n");
					scanf("%d",&zenbakia);
					printf("Sartu kolore gorriaren intentsitatea\n");
					scanf("%d",&R);
					printf("Sartu kolore berdearen intentsitatea\n");
					scanf("%d",&G);
					printf("Sartu kolore urdinaren intentsitatea\n");
					scanf("%d",&B);
					sprintf(buf,"%s %d;%d;%d;%d\r\n",KOMANDOAK[4],R,G,B,zenbakia);
					idatzi(socketa,buf);
					n=irakurri(socketa,buf);
					buf[n-2]=0;
					if(0==strncmp(ERANTZUNAK[0],buf,2))
					{
						printf("Bonbilaren kolorea aldatu da\n");
					}else if(0==strncmp(ERANTZUNAK[1],buf,3)){
						printf("Sartutako bonbila zenbakia ez da existitzen D:\n");
					}else if(0==strncmp(ERANTZUNAK[4],buf,3)){
						printf("Sartutako RGB balioetakorenbat edo gehiago ez da 0 eta 255 artekoa\n");
					}
					break;
			
				case 4:
					printf("Sartu aldatu nahi duzun argiaren zenbakia:\n");
					scanf("%d",&zenbakia);
					printf("Sartu argiaren intentsitatea\n");
					scanf("%d",&ind);
					sprintf(buf,"%s %d;%d\r\n",KOMANDOAK[5],ind,zenbakia);
					idatzi(socketa,buf);
					n=irakurri(socketa,buf);
					buf[n-2]=0;
					if(0==strncmp(ERANTZUNAK[0],buf,2)){
						printf("Bonbilaren intentsitatea aldatu da\n");
					}else if(0==strncmp(ERANTZUNAK[1],buf,3)){
						printf("Sartutako bonbila zenbakia ez da existitzen D:\n");
					}else if(0==strncmp(ERANTZUNAK[4],buf,3)){
						printf("Sartutako intentsitatearen balioa ez dago 0 eta 100 artean\n");
					}
					break;		
				case 5:
					printf("Sartu egoera ikusi nahi duzun bonbilaren zenbakia:");
					scanf("%d",&bonbzen);
					sprintf(buf,"%s %d\r\n",KOMANDOAK[6], bonbzen);
					idatzi(socketa,buf);	
					if((n = read(socketa, buf, 25)) < 0)
					{
						perror("Errorea datuak jasotzean");
						close(socketa);
					}	
					buf[n-2]=0;
					token = strtok(buf," ");
					strcpy(erantzun,token);
					if(0==strncmp(ERANTZUNAK[0],buf,2))
					{
						printf("Hau da %d bonbilaren egoera:\n", bonbzen);
						token = strtok(NULL," ");
						token = strtok(NULL," ");
						strcpy(erantzun,token);
						printf("itzalita=0; piztuta=1 :\n");
						printf("%s\n",token);
						token = strtok(NULL,";");
						strcpy(erantzun,token);
						printf("Gorriaren intentsitatea:\n");
						printf("%s\n",token);
						token = strtok(NULL,";");
						strcpy(erantzun,token);
						printf("Berdearen intentsitatea:\n");
						printf("%s\n",token);
						token = strtok(NULL," ");
						strcpy(erantzun,token);
						printf("Urdinaren intentsitatea:\n");
						printf("%s\n",token);
						token = strtok(NULL,";");
						strcpy(erantzun,token);
						printf("Argiaren intentsitatea:\n");
						printf("%s\n",token);
					}
					break;
			
					case 0:
						printf("Aplikaziotik irteten...\n");
						sprintf(buf,"%s\r\n",KOMANDOAK[1]);
						idatzi(socketa,buf);
						irten=1;
						break;
					default:
						printf("Sar ezazu zenbaki zuzen bat");
		}
	}	
	}else if(0==strncmp(ERANTZUNAK[6],buf,3)){
		printf("Erabiltzaile hori beste makina batean konektatuta dago");	
	}else{
		printf("Erabiltzaile edo pasahitz okerra");
	}
	close(socketa);
	exit(0);
}






